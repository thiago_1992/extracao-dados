# Extração dados README v1.0.0

## Objetivo deste trabalho

O script irá percorrer a página informada (default: 'http://g1.globo.com/economia/mercados/cotacoes/moedas/'), transformar em um objeto e trabalhar seus dados exportando-o para um .csv. Destarte utilizamos a linguagem python para mostrar como é feita a leitura de dados de uma página e sua utilização em um mundo real. Há diferentes formas de se utilizar o script, uma delas é adicionar ao um 'cron' que ficaria de tempos em tempos acompanhando o resultado encontrado e compará-los em um gráfico a partir de um csv ou afins.

1) Instalação do python (Linux)

	# apt-get update
	# apt-get install python -y
	# sudo apt-get install bypthon -y (opcional)
	
	Obs.: Caso todas as bibliotecas não tenham sido instaladas utilize o comando: sudo apt-get -f install -y
	
2) Instalação do urllib2
	
	# apt-get install python-urllib3  # for python 2
	# apt-get install python3-urllib3 # for python 3
 
* Utilização no código

	import urllib2  # Lib responsavel por acessar a pagina
 
+ Documentação: https://docs.python.org/2/library/urllib2.html

3) Instalação do Beautiful Soup
	
	# apt-get install python-pip -y
	# pip install beautifulsoup4

* Sobre o pip: http://www.devfuria.com.br/linux/instalando-pip/  
* Documentação: https://www.crummy.com/software/BeautifulSoup/

4) Execução do script

	$ python analise-dados.py	
	
# Notas
Recomendamos o desenvolvedor realizar um fork do projeto e acrescentar suas ideias ao script

==============================================================================================================

# Extract data README v1.0.0

## Of This Work

The script will scroll through the page you entered (default: 'http://g1.globo.com/economy/mercados/cotacoes/moedas/'), transform into an object and work your data by exporting it to a .csv. So we use the python language to show how to read data from a page and its use in a real world. There are different ways to use the script, one of them is to add to a 'cron' that would be from time to time following the result found and compare them in a chart from a csv or the like.

1) Intall python (Linux)

	# apt-get update
	# apt-get install python -y
	# sudo apt-get install bypthon -y (opcional)
	
	Obs.: If all the libraries have not been installed use the command: sudo apt-get -f install -y
	
2) Install urllib2 (lib python)
	
	# apt-get install python-urllib3  # for python 2
	# apt-get install python3-urllib3 # for python 3
 
* use to code

	import urllib2  # Lib responsavel por acessar a pagina
 
+ Documentation: https://docs.python.org/2/library/urllib2.html

3) Install Beautiful Soup
	
	# apt-get install python-pip -y
	# pip install beautifulsoup4

* About pip: http://www.devfuria.com.br/linux/instalando-pip/  
* Documentation: https://www.crummy.com/software/BeautifulSoup/

4) Exec

	$ python analise-dados.py	
	
# notice
We recommend that the developer build a project fork and add their ideas to the script
